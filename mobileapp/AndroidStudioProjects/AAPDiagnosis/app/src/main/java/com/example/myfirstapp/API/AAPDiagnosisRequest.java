package com.example.myfirstapp.API;

public class AAPDiagnosisRequest {
    public static String AAP_BASE_REQUEST = "{" +
            "\"sex\": [], " +
            "\"age\": [], " +
            "\"site of pain at onset\": [], " +
            "\"site of pain at present\": []," +
            "\"aggravating factors\": [], " +
            "\"relieving factors\": [], " +
            "\"progress\": [], " +
            "\"duration\": [], " +
            "\"type\": [], " +
            "\"severity\": [], " +
            "\"nausea\": [], " +
            "\"vomiting\": [], " +
            "\"anorexia\": [], " +
            "\"previous indigestion\": [], " +
            "\"jaundice\": [], " +
            "\"bowels\": [], " +
            "\"micturition\": [], " +
            "\"previous similar pain\": [], " +
            "\"previous abdo surgery\": [], " +
            "\"drugs for abdo pain\": [], " +
            "\"mood\": [], " +
            "\"colour\": [], " +
            "\"abdo movement\": [], " +
            "\"scar\": [], " +
            "\"distension\": [], " +
            "\"site of tenderness\": [], " +
            "\"rebound\": [], " +
            "\"guarding\": [], " +
            "\"rigidity\": [], " +
            "\"mass\": [], " +
            "\"murphy\": [], " +
            "\"bowel\": [], " +
            "\"rectal tenderness\": []" +
    "}";

    public static String AAP_GYN_BASE_REQUEST = "{" +
            "\"sex\": [], " +
            "\"age\": [], " +
            "\"site of pain at onset\": [], " +
            "\"site of pain at present\": [], " +
            "\"aggravating factors\": [], " +
            "\"relieving factors\": [], " +
            "\"progress\": [], " +
            "\"duration\": [], " +
            "\"type\": [], " +
            "\"severity\": [], " +
            "\"nausea\": [], " +
            "\"vomiting\": [], " +
            "\"anorexia\": [], " +
            "\"previous indigestion\": [], " +
            "\"jaundice\": [], " +
            "\"bowels\": [], " +
            "\"micturition\": [], " +
            "\"previous similar pain\": [], " +
            "\"previous abdo surgery\": [], " +
            "\"drugs for abdo pain\": [], " +
            "\"mood\": [], " +
            "\"colour\": [], " +
            "\"abdo movement\": [], " +
            "\"scar\": [], " +
            "\"distension\": [], " +
            "\"site of tenderness\": [], " +
            "\"rebound\": [], " +
            "\"guarding\": [], " +
            "\"rigidity\": [], " +
            "\"mass\": [], " +
            "\"murphy\": [], " +
            "\"bowel\": [], " +
            "\"rectal tenderness\": [], " +
            "\"periods\": [], " +
            "\"last monthly period\": [], " +
            "\"vaginal discharge\": [], " +
            "\"pregnancy\": [], " +
            "\"faint/dizzy\": [], " +
            "\"previous history of salpingitis/std\": [], " +
            "\"vaginal tenderness\": []" +
    "}";
}